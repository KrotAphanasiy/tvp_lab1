#ifndef DATA_GENERATOR_CPP
#define DATA_GENERATOR_CPP

#include <random>

#include "DataGenerator.h"

std::tr1::uniform_int<Type> uniform(
	std::numeric_limits<Type>::min(),
	std::numeric_limits<Type>::max());
std::mt19937_64 engine;

void generateRandomNumbers(std::vector<Type>* vec) {
	std::for_each((*vec).begin(), (*vec).end(), [](Type& i) { i = uniform(engine); });
}

#endif