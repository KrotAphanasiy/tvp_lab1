#ifndef THREADED_QUICKSORT_H
#define THREADED_QUICKSORT_H

#include <vector>

typedef __int64 Type;

void threadedQuickSort(std::vector<Type>* vec);

int quickSort(std::vector<Type>::iterator begin, std::vector<Type>::iterator end);

#endif