#ifndef THREADED_QUICKSORT_CPP
#define THREADED_QUICKSORT_CPP

#include <future>
#include "ThreadedQuicksort.h"

int nativeQuickSort(std::vector<Type>::iterator begin, std::vector<Type>::iterator end) {
    auto const sz = end - begin;
    if (sz <= 1) return 0;

    auto pivot = begin + sz / 2;
    auto const pivot_v = *pivot;

    std::swap(*pivot, *(end - 1));
    auto p = std::partition(begin, end, [&](const Type& a) { return a < pivot_v; });
    std::swap(*p, *(end - 1));

    if (sz > 4096) {
        auto left = std::async(std::launch::async, [&]() {
            return nativeQuickSort(begin, p);
            });
        nativeQuickSort(p + 1, end);
    }
    else {
        nativeQuickSort(begin, p);
        nativeQuickSort(p + 1, end);
    }
    return 0;
}

int quickSort(std::vector<Type>::iterator begin, std::vector<Type>::iterator end) {
    auto const sz = end - begin;
    if (sz <= 1) return 0;

    auto pivot = begin + sz / 2;
    auto const pivot_v = *pivot;

    std::swap(*pivot, *(end - 1));
    auto p = std::partition(begin, end, [&](const Type& a) { return a < pivot_v; });
    std::swap(*p, *(end - 1));

    nativeQuickSort(begin, p);
    nativeQuickSort(p + 1, end);

    return 0;
}

void threadedQuickSort(std::vector<Type>* vec) {
    nativeQuickSort((*vec).begin(), (*vec).end());
}

#endif