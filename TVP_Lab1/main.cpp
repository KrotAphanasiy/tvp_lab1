#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <chrono>
#include <algorithm>

#include "ThreadedQuicksort.h"
#include "DataGenerator.h"

const int ITERATIONS_NUM = 10;
const int DATA_SIZE = 1000000;

int main(int argc, char* argv[]) {
    std::vector<double> times;
    auto times_sum = 0.0;

    for (auto i = 0; i < ITERATIONS_NUM; ++i) {
        std::vector<Type>* dataVecPtr = new std::vector<Type>(DATA_SIZE);

        generateRandomNumbers(dataVecPtr);

        std::cout << "Started sorting, try " << i << std::endl;
        auto start = std::chrono::high_resolution_clock::now();

        threadedQuickSort(dataVecPtr);

        auto stop = std::chrono::high_resolution_clock::now();
        std::cout << "Stopped sorting" << std::endl;
        auto duration = std::chrono::duration<double>(stop - start).count();
        std::cout << duration;

        times.push_back(duration);
        times_sum += duration;

        std::cout << "\n";
        delete dataVecPtr;
        dataVecPtr = nullptr;
    }

    auto average = times_sum / ITERATIONS_NUM;
    auto max_element = *std::max_element(times.begin(), times.end());
    auto min_element = *std::min_element(times.begin(), times.end());
    auto average_fixed = (times_sum - max_element - min_element) /
        (ITERATIONS_NUM - 2);

    std::cout << "THREADED QUICKSORT:\nAverage: " << average << "s, "
        << "Average without max/min: "
        << average_fixed << "s." << std::endl;

    times_sum = 0;
    times.clear();

    for (auto i = 0; i < ITERATIONS_NUM; ++i) {
        std::vector<Type>* dataVecPtr = new std::vector<Type>(DATA_SIZE);

        generateRandomNumbers(dataVecPtr);

        std::cout << "Started sorting, try " << i << std::endl;
        auto start = std::chrono::high_resolution_clock::now();

        quickSort(dataVecPtr->begin(), dataVecPtr->end());

        auto stop = std::chrono::high_resolution_clock::now();
        std::cout << "Stopped sorting" << std::endl;
        auto duration = std::chrono::duration<double>(stop - start).count();
        std::cout << duration;

        times.push_back(duration);
        times_sum += duration;

        std::cout << "\n";
        delete dataVecPtr;
        dataVecPtr = nullptr;
    }

    average = times_sum / ITERATIONS_NUM;
    max_element = *std::max_element(times.begin(), times.end());
    min_element = *std::min_element(times.begin(), times.end());
    average_fixed = (times_sum - max_element - min_element) /
        (ITERATIONS_NUM - 2);

    std::cout << "SINGLE THREAD QUICKSORT:\nAverage: " << average << "s, "
        << "Average without max/min: "
        << average_fixed << "s." << std::endl;

	return 0;
}